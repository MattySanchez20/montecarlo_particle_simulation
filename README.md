# Testing the Feasibility of Hyperon-Nucleon Interaction Experiments at Jefferson Lab Using A Monte Carlo Simulation (and Links to Neutron Stars)


## Abstract

An experiment has been proposed to use the GlueX detector (in Hall D, Jefferson Lab) to
measure the lesser-known cross section of the Λp re-scattering events arising from the
following interaction: KL
0p → Λ π
+, Λp → Λ
′p
′
, Λ
′ → pπ
− (Λ[uds]). To test the feasibility
of the proposed experiment, a Monte Carlo Simulation was used to calculate the yield of the
interaction. The simulation obtained the luminosity and the acceptance of the GlueX detector
(0.336 ± 0.012); however, the cross sections were simulated using previous data points from
past experiments. The simulation accounted for the decay of Λ and for regions within the
detector where events could not be measured; the decay decreased the luminosity, and the dead
space within the detector decreased the acceptance. The yield of the experiment was 42K ±
10K Λp re-scattering events, which provides sufficient data to calculate the cross section of the
Λp re-scattering event. Thus, the experiment is feasible.

Full report available [here](https://drive.google.com/file/d/1A6aAWS00JkB9HWMMaROHBs-Opmutzf95/view).
